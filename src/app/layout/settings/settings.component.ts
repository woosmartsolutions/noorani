import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Router} from '@angular/router';
import {MatCheckboxChange, MatRadioChange} from '@angular/material';
import {DataService} from 'src/app/services/data.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SettingsComponent implements OnInit {

  isLessonActive = false;
  settingsFormGroup: FormGroup;
  isPlaying = false;
  isFullScreen = true;
  isReadingWithTeacher = false;
  rangeButtonText = '';
  repeatCount = '';
  isRangeClicked = false;
  isStopped = true;

  constructor(private router: Router, private fb: FormBuilder,
              private dataService: DataService) {
  }

  ngOnInit() {
    this.setFormField();
    this.getPreferencesValue();
    this.rangeButtonText = 'Determine the beginning and the end of reading';
    this.dataService.getLessonStatus().subscribe(status => {
      console.log(status);
      this.isLessonActive = status;
    });
    if (localStorage.getItem('isLessonsActive') === 'true') {
      this.isLessonActive = true;
    } else {
      this.isLessonActive = false;
    }
  }

  setFormField() {
    this.settingsFormGroup = this.fb.group({
      fullPage: [''],
      readTeacher: [''],
      repeatCount: [''],
      spelling: [''],
      language: ['']
    });
  }

  playAudio() {
    if (this.isPlaying) {
      this.isPlaying = false;
    } else {
      this.isPlaying = true;
    }
  }

  getPreferencesValue(): void {
    console.log(JSON.stringify(this.settingsFormGroup.value));
  }

  onFullScreenToggle(event: MatCheckboxChange) {
    console.log('Full Screen Toggle: ' + event.checked);
  }

  onReadingWithTeacherToggle(event: MatCheckboxChange) {
    console.log('Reading With Teacher Toggle: ' + event.checked);
  }

  onReadingChange(event: MatRadioChange) {
    console.log('Reading Preferences: ' + event);
  }

  onLanguageChange(event: MouseEvent) {
    console.log('Language Preferences: ' + event);
  }

  onSubmit() {
    console.log(JSON.stringify(this.settingsFormGroup.value));
  }

  onPlay(repeatCount) {
    this.isPlaying = !this.isPlaying;
    this.dataService.sendRepeatCount(repeatCount);
    localStorage.setItem('repeatCount', this.repeatCount);
    localStorage.setItem('isPlaying', String(this.isPlaying));
    if (this.isPlaying) {
      this.isStopped = false;
      this.dataService.onStopClicked(this.isStopped);
    } else {

    }
  }

  onRangeClick() {
    this.isRangeClicked = !this.isRangeClicked;
    this.dataService.sendData(this.isRangeClicked);
  }

  onRepeatCountChange(value: string) {
    console.log(value);
  }

  onStop() {
    if (this.isPlaying) {
      this.isPlaying = false;
    }
    this.dataService.onStopClicked(this.isStopped);
  }
}
