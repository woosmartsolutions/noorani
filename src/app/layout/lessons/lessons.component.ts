import {Component, OnInit} from '@angular/core';
import {RouteConfig} from '../../common/route.config';
import {Router} from '@angular/router';
import {DataService} from '../../services/data.service';

@Component({
  selector: 'app-lessons',
  templateUrl: './lessons.component.html',
  styleUrls: ['./lessons.component.scss']
})
export class LessonsComponent implements OnInit {

  actionLinks = RouteConfig;
  isLessonsActive = false;

  constructor(private router: Router, private dataService: DataService) {
  }

  ngOnInit() {
    this.dataService.sendLesson(this.isLessonsActive);
    this.setRouterId();
    if (this.isLessonsActive) {
      localStorage.setItem('isLessonsActive', String(this.isLessonsActive));
    }
  }

  navigateToUrl(url) {
    console.log(url);
    this.router.navigate(['home/' + url]);
  }

  onHover(event: MouseEvent) {
    if (event.returnValue) {
      const hoverAudio = document.getElementById('hoverEffect') as HTMLAudioElement;
      hoverAudio.play();
    }
  }

  onLessonClick() {
    this.isLessonsActive = true;
    this.dataService.sendLesson(this.isLessonsActive);
  }

  setRouterId(counter?: number) {

    /*if (classList.prevObject.length === 1) {
      this.isLessonsActive = true;
      this.dataService.sendLesson(this.isLessonsActive);
    } else {
      this.isLessonsActive = false;
      this.dataService.sendLesson(this.isLessonsActive);
    }*/
  }
}
