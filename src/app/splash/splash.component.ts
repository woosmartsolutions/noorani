import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import * as AOS from 'aos';

@Component({
  selector: 'app-splash',
  templateUrl: './splash.component.html',
  styleUrls: ['./splash.component.scss']
})
export class SplashComponent implements OnInit {

  constructor(private router: Router) {
  }

  ngOnInit() {
    AOS.init();
    this.goToIntro();
  }

  goToIntro(): void {
    setTimeout(() => {
      this.router.navigate(['intro']).then(r => console.log(r));
    }, 3000);
  }
}
