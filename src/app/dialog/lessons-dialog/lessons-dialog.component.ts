import {Component, Inject, OnInit} from '@angular/core';
import {RouteConfig} from '../../common/route.config';
import {Router} from '@angular/router';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-lessons-dialog',
  templateUrl: './lessons-dialog.component.html',
  styleUrls: ['./lessons-dialog.component.scss']
})
export class LessonsDialogComponent implements OnInit {

  actionLinks = RouteConfig;

  constructor(private router: Router,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private dialogRef: MatDialogRef<LessonsDialogComponent>) {
  }

  ngOnInit() {
  }

  navigateToUrl(url) {
    console.log(url);
    this.dialogRef.afterClosed().subscribe(res => {
      console.log(res);
      this.router.navigate(['home/' + url]);
    });
    this.dialogRef.close(url);
  }

}
