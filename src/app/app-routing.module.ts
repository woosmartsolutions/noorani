import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {IntroductionComponent} from './introduction/introduction.component';
import {SplashComponent} from './splash/splash.component';
import {MainContentComponent} from './layout/main-content/main-content.component';
import {HomeComponent} from './home/home.component';


export const routes: Routes = [
  {
    path: '', component: SplashComponent
  },
  {
    path: 'intro', component: IntroductionComponent,
  },
  {
    path: 'home', component: HomeComponent,
    children: [
      {
        path: '',
        loadChildren: './home/lessons.module#LessonsModule'
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
