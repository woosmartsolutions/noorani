export class Constants {
  static LETTER_IMAGE_URL = 'assets/images/qaida';
  static LETTER_IMAGE_URL_ONE = 'assets/images/qaida/lesson1';
  static LETTER_IMAGE_URL_TWO = 'assets/images/qaida/lesson2';
  static LETTER_IMAGE_URL_THREE = 'assets/images/qaida/lesson3';
  static LETTER_IMAGE_URL_FOUR = 'assets/images/qaida/lesson4/';
  static LETTER_IMAGE_URL_FIVE = 'assets/images/qaida/lesson5/';
  static LETTER_IMAGE_URL_SIX = 'assets/images/qaida/lesson6/';
  static LETTER_IMAGE_URL_SEVEN = 'assets/images/qaida/lesson7/';
  static LETTER_IMAGE_URL_EIGHT = 'assets/images/qaida/lesson8';
  static LETTER_IMAGE_URL_NINE = 'assets/images/qaida/lesson9';
  static LETTER_IMAGE_URL_TEN = 'assets/images/qaida/lesson10';
  static LETTER_IMAGE_URL_ELEVEN = 'assets/images/qaida/lesson11';
  static LETTER_IMAGE_URL_TWELEVE = 'assets/images/qaida/lesson12';
  static LETTER_IMAGE_URL_THIRTEEN = 'assets/images/qaida/lesson13';
  static LETTER_IMAGE_URL_FOURTEEN = 'assets/images/qaida/lesson14';
  static LETTER_IMAGE_URL_FIFTEEN = 'assets/images/qaida/lesson15';
  static LETTER_IMAGE_URL_SIXTEEN = 'assets/images/qaida/lesson16';
  static LETTER_IMAGE_URL_SEVENTEEN = 'assets/images/qaida/lesson17';
}

export const RouteConfig = [
  {
    id: 1,
    title: 'Lesson 1',
    url: 'lesson-one'
  },
  {
    id: 2,
    title: 'Lesson 2',
    url: 'lesson-two'
  },
  {
    id: 3,
    title: 'Lesson 3',
    url: 'lesson-three'
  },
  {
    id: 4,
    title: 'Lesson 4',
    url: 'lesson-four'
  },
  {
    id: 5,
    title: 'Lesson 5',
    url: 'lesson-five'
  },
  {
    id: 6,
    title: 'Lesson 6',
    url: 'lesson-six'
  },
  {
    id: 7,
    title: 'Lesson 7',
    url: 'lesson-seven'
  },
  {
    id: 8,
    title: 'Lesson 8',
    url: 'lesson-eight'
  },
  {
    id: 9,
    title: 'Lesson 9',
    url: 'lesson-nine'
  },
  {
    id: 10,
    title: 'Lesson 10',
    url: 'lesson-ten'
  },
  {
    id: 11,
    title: 'Lesson 11',
    url: 'lesson-eleven'
  },
  {
    id: 12,
    title: 'Lesson 12',
    url: 'lesson-twelve'
  },
  {
    id: 13,
    title: 'Lesson 13',
    url: 'lesson-thirteen'
  },
  {
    id: 14,
    title: 'Lesson 14',
    url: 'lesson-fourteen'
  },
  {
    id: 15,
    title: 'Lesson 15',
    url: 'lesson-fifteen'
  },
  {
    id: 16,
    title: 'Lesson 16',
    url: 'lesson-sixteen'
  },
  {
    id: 17,
    title: 'Lesson 17',
    url: 'lesson-seventeen'
  },
];
