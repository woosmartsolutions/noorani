import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule, routes} from './app-routing.module';
import {AppComponent} from './app.component';
import {SettingsComponent} from './layout/settings/settings.component';
import {MainContentComponent} from './layout/main-content/main-content.component';
import {LessonsComponent} from './layout/lessons/lessons.component';
import {SplashComponent} from './splash/splash.component';
import {HomeComponent} from './home/home.component';
import {IntroductionComponent} from './introduction/introduction.component';
import {RouterModule} from '@angular/router';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MatButtonModule,
  MatButtonToggleModule,
  MatCheckboxModule,
  MatDialogModule, MatDividerModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatRadioModule,
  MatSidenavModule,
  MatToolbarModule
} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {LessonsModule} from './home/lessons.module';
import { Lesson0Component } from './pages/lesson0/lesson0.component';

@NgModule({
  declarations: [
    AppComponent,
    SettingsComponent,
    MainContentComponent,
    LessonsComponent,
    SplashComponent,
    HomeComponent,
    IntroductionComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(routes),
    BrowserAnimationsModule,
    MatRadioModule,
    MatCheckboxModule,
    MatFormFieldModule,
    FormsModule,
    MatInputModule,
    MatButtonToggleModule,
    MatIconModule,
    MatButtonModule,
    ReactiveFormsModule,
    LessonsModule,
    MatToolbarModule,
    MatSidenavModule,
    MatDialogModule,
    MatDividerModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
  exports: [
    LessonsComponent
  ],
  entryComponents: []
})
export class AppModule {
}
