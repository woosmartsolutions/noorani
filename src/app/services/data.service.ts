import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private onStop = new BehaviorSubject('');
  onStopStatus = this.onStop.asObservable();

  private srcMsg = new BehaviorSubject('');
  tMsg = this.srcMsg.asObservable();
  private repeatCount = new BehaviorSubject('');
  repeatCountValue = this.repeatCount.asObservable();
  private isLessonActive = new BehaviorSubject('');
  lessActive = this.isLessonActive.asObservable();

  constructor() {
  }

  sendData(data: any) {
    this.srcMsg.next(data);
  }

  getData(): Observable<any> {
    return this.tMsg;
  }

  sendLesson(lessonToggle: any) {
    this.isLessonActive.next(lessonToggle);
  }

  getLessonStatus(): Observable<any> {
    return this.lessActive;
  }

  sendRepeatCount(value: any) {
    this.repeatCount.next(value);
  }

  getRepeatCount(): Observable<any> {
    return this.repeatCountValue;
  }

  onStopClicked(isStop: any) {
    this.onStop.next(isStop);
  }

  getOnStopStatus(): Observable<any> {
    return this.onStopStatus;
  }
}
