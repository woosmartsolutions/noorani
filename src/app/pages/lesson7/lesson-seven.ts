import {Constants} from '../../common/route.config';

export const LessonSevenSixLetterData = [
  {
    id: 6,
    name: 'one',
    url: Constants.LETTER_IMAGE_URL_SEVEN + '6' + '.png',
    audioUrl: Constants.LETTER_IMAGE_URL_SEVEN + 'audio/' + 'P13.2.1.1.wav'
  },
  {
    id: 5,
    name: 'one',
    url: Constants.LETTER_IMAGE_URL_SEVEN + '5' + '.png',
    audioUrl: Constants.LETTER_IMAGE_URL_SEVEN + 'audio/' + 'P13.2.2.2.wav'
  },
  {
    id: 4,
    name: 'one',
    url: Constants.LETTER_IMAGE_URL_SEVEN + '4' + '.png',
    audioUrl: Constants.LETTER_IMAGE_URL_SEVEN + 'audio/' + 'P13.2.2.1.wav'
  },
  {
    id: 3,
    name: 'one',
    url: Constants.LETTER_IMAGE_URL_SEVEN + '3' + '.png',
    audioUrl: Constants.LETTER_IMAGE_URL_SEVEN + 'audio/' + 'P13.2.2.2.wav'
  },
  {
    id: 2,
    name: 'one',
    url: Constants.LETTER_IMAGE_URL_SEVEN + '2' + '.png',
    audioUrl: Constants.LETTER_IMAGE_URL_SEVEN + 'audio/' + 'P13.2.2.3.wav'
  },
  {
    id: 1,
    name: 'one',
    url: Constants.LETTER_IMAGE_URL_SEVEN + '1' + '.png',
    audioUrl: Constants.LETTER_IMAGE_URL_SEVEN + 'audio/' + 'P13.2.2.4.wav'
  },
  {
    id: 12,
    name: 'one',
    url: Constants.LETTER_IMAGE_URL_SEVEN + '12' + '.png',
    audioUrl: Constants.LETTER_IMAGE_URL_SEVEN + 'audio/' + 'P13.2.2.5.wav'
  },
  {
    id: 11,
    name: 'one',
    url: Constants.LETTER_IMAGE_URL_SEVEN + '11' + '.png',
    audioUrl: Constants.LETTER_IMAGE_URL_SEVEN + 'audio/' + 'P13.2.2.6.wav'
  },
  {
    id: 10,
    name: 'one',
    url: Constants.LETTER_IMAGE_URL_SEVEN + '10' + '.png',
    audioUrl: Constants.LETTER_IMAGE_URL_SEVEN + 'audio/' + 'P13.2.2.7.wav'
  },
  {
    id: 9,
    name: 'one',
    url: Constants.LETTER_IMAGE_URL_SEVEN + '9' + '.png',
    audioUrl: Constants.LETTER_IMAGE_URL_SEVEN + 'audio/' + 'hover.mp3'
  },
  {
    id: 8,
    name: 'one',
    url: Constants.LETTER_IMAGE_URL_SEVEN + '8' + '.png',
    audioUrl: Constants.LETTER_IMAGE_URL_SEVEN + 'audio/' + 'hover.mp3'
  },
  {
    id: 7,
    name: 'one',
    url: Constants.LETTER_IMAGE_URL_SEVEN + '7' + '.png',
    audioUrl: Constants.LETTER_IMAGE_URL_SEVEN + 'audio/' + 'hover.mp3'
  },
  {
    id: 18,
    name: 'one',
    url: Constants.LETTER_IMAGE_URL_SEVEN + '18' + '.png',
    audioUrl: Constants.LETTER_IMAGE_URL_SEVEN + 'audio/' + 'hover.mp3'
  },
  {
    id: 17,
    name: 'one',
    url: Constants.LETTER_IMAGE_URL_SEVEN + '17' + '.png',
    audioUrl: Constants.LETTER_IMAGE_URL_SEVEN + 'audio/' + 'hover.mp3'
  },
  {
    id: 16,
    name: 'one',
    url: Constants.LETTER_IMAGE_URL_SEVEN + '16' + '.png',
    audioUrl: Constants.LETTER_IMAGE_URL_SEVEN + 'audio/' + 'hover.mp3'
  },
  {
    id: 15,
    name: 'one',
    url: Constants.LETTER_IMAGE_URL_SEVEN + '15' + '.png',
    audioUrl: Constants.LETTER_IMAGE_URL_SEVEN + 'audio/' + 'hover.mp3'
  },
  {
    id: 14,
    name: 'one',
    url: Constants.LETTER_IMAGE_URL_SEVEN + '14' + '.png',
    audioUrl: Constants.LETTER_IMAGE_URL_SEVEN + 'audio/' + 'hover.mp3'
  },
  {
    id: 13,
    name: 'one',
    url: Constants.LETTER_IMAGE_URL_SEVEN + '13' + '.png',
    audioUrl: Constants.LETTER_IMAGE_URL_SEVEN + 'audio/' + 'hover.mp3'
  },

];

export const LessonSevenSevenLetterData = [
  {
    id: 25,
    name: 'one',
    url: Constants.LETTER_IMAGE_URL_SEVEN + '25' + '.png',
    audioUrl: Constants.LETTER_IMAGE_URL_SEVEN + 'audio/' + 'hover.mp3'
  },
  {
    id: 24,
    name: 'one',
    url: Constants.LETTER_IMAGE_URL_SEVEN + '24' + '.png',
    audioUrl: Constants.LETTER_IMAGE_URL_SEVEN + 'audio/' + 'hover.mp3'
  },
  {
    id: 23,
    name: 'one',
    url: Constants.LETTER_IMAGE_URL_SEVEN + '23' + '.png',
    audioUrl: Constants.LETTER_IMAGE_URL_SEVEN + 'audio/' + 'hover.mp3'
  },
  {
    id: 22,
    name: 'one',
    url: Constants.LETTER_IMAGE_URL_SEVEN + '22' + '.png',
    audioUrl: Constants.LETTER_IMAGE_URL_SEVEN + 'audio/' + 'hover.mp3'
  },
  {
    id: 21,
    name: 'one',
    url: Constants.LETTER_IMAGE_URL_SEVEN + '21' + '.png',
    audioUrl: Constants.LETTER_IMAGE_URL_SEVEN + 'audio/' + 'hover.mp3'
  },
  {
    id: 20,
    name: 'one',
    url: Constants.LETTER_IMAGE_URL_SEVEN + '20' + '.png',
    audioUrl: Constants.LETTER_IMAGE_URL_SEVEN + 'audio/' + 'hover.mp3'
  },
  {
    id: 19,
    name: 'one',
    url: Constants.LETTER_IMAGE_URL_SEVEN + '19' + '.png',
    audioUrl: Constants.LETTER_IMAGE_URL_SEVEN + 'audio/' + 'hover.mp3'
  },
  {
    id: 32,
    name: 'one',
    url: Constants.LETTER_IMAGE_URL_SEVEN + '32' + '.png',
    audioUrl: Constants.LETTER_IMAGE_URL_SEVEN + 'audio/' + 'hover.mp3'
  },
  {
    id: 31,
    name: 'one',
    url: Constants.LETTER_IMAGE_URL_SEVEN + '31' + '.png',
    audioUrl: Constants.LETTER_IMAGE_URL_SEVEN + 'audio/' + 'hover.mp3'
  },
  {
    id: 30,
    name: 'one',
    url: Constants.LETTER_IMAGE_URL_SEVEN + '30' + '.png',
    audioUrl: Constants.LETTER_IMAGE_URL_SEVEN + 'audio/' + 'hover.mp3'
  },
  {
    id: 29,
    name: 'one',
    url: Constants.LETTER_IMAGE_URL_SEVEN + '29' + '.png',
    audioUrl: Constants.LETTER_IMAGE_URL_SEVEN + 'audio/' + 'hover.mp3'
  },
  {
    id: 28,
    name: 'one',
    url: Constants.LETTER_IMAGE_URL_SEVEN + '28' + '.png',
    audioUrl: Constants.LETTER_IMAGE_URL_SEVEN + 'audio/' + 'hover.mp3'
  },
  {
    id: 27,
    name: 'one',
    url: Constants.LETTER_IMAGE_URL_SEVEN + '27' + '.png',
    audioUrl: Constants.LETTER_IMAGE_URL_SEVEN + 'audio/' + 'hover.mp3'
  },
  {
    id: 26,
    name: 'one',
    url: Constants.LETTER_IMAGE_URL_SEVEN + '26' + '.png',
    audioUrl: Constants.LETTER_IMAGE_URL_SEVEN + 'audio/' + 'hover.mp3'
  },
  {
    id: 39,
    name: 'one',
    url: Constants.LETTER_IMAGE_URL_SEVEN + '39' + '.png',
    audioUrl: Constants.LETTER_IMAGE_URL_SEVEN + 'audio/' + 'hover.mp3'
  },
  {
    id: 38,
    name: 'one',
    url: Constants.LETTER_IMAGE_URL_SEVEN + '38' + '.png',
    audioUrl: Constants.LETTER_IMAGE_URL_SEVEN + 'audio/' + 'hover.mp3'
  },
  {
    id: 37,
    name: 'one',
    url: Constants.LETTER_IMAGE_URL_SEVEN + '37' + '.png',
    audioUrl: Constants.LETTER_IMAGE_URL_SEVEN + 'audio/' + 'hover.mp3'
  },
  {
    id: 36,
    name: 'one',
    url: Constants.LETTER_IMAGE_URL_SEVEN + '36' + '.png',
    audioUrl: Constants.LETTER_IMAGE_URL_SEVEN + 'audio/' + 'hover.mp3'
  },
  {
    id: 35,
    name: 'one',
    url: Constants.LETTER_IMAGE_URL_SEVEN + '35' + '.png',
    audioUrl: Constants.LETTER_IMAGE_URL_SEVEN + 'audio/' + 'hover.mp3'
  },
  {
    id: 34,
    name: 'one',
    url: Constants.LETTER_IMAGE_URL_SEVEN + '34' + '.png',
    audioUrl: Constants.LETTER_IMAGE_URL_SEVEN + 'audio/' + 'hover.mp3'
  },
  {
    id: 33,
    name: 'one',
    url: Constants.LETTER_IMAGE_URL_SEVEN + '33' + '.png',
    audioUrl: Constants.LETTER_IMAGE_URL_SEVEN + 'audio/' + 'hover.mp3'
  },
  {
    id: 46,
    name: 'one',
    url: Constants.LETTER_IMAGE_URL_SEVEN + '46' + '.png',
    audioUrl: Constants.LETTER_IMAGE_URL_SEVEN + 'audio/' + 'hover.mp3'
  },
  {
    id: 45,
    name: 'one',
    url: Constants.LETTER_IMAGE_URL_SEVEN + '45' + '.png',
    audioUrl: Constants.LETTER_IMAGE_URL_SEVEN + 'audio/' + 'hover.mp3'
  },
  {
    id: 44,
    name: 'one',
    url: Constants.LETTER_IMAGE_URL_SEVEN + '44' + '.png',
    audioUrl: Constants.LETTER_IMAGE_URL_SEVEN + 'audio/' + 'hover.mp3'
  },
  {
    id: 43,
    name: 'one',
    url: Constants.LETTER_IMAGE_URL_SEVEN + '43' + '.png',
    audioUrl: Constants.LETTER_IMAGE_URL_SEVEN + 'audio/' + 'hover.mp3'
  },
  {
    id: 42,
    name: 'one',
    url: Constants.LETTER_IMAGE_URL_SEVEN + '42' + '.png',
    audioUrl: Constants.LETTER_IMAGE_URL_SEVEN + 'audio/' + 'hover.mp3'
  },
  {
    id: 41,
    name: 'one',
    url: Constants.LETTER_IMAGE_URL_SEVEN + '41' + '.png',
    audioUrl: Constants.LETTER_IMAGE_URL_SEVEN + 'audio/' + 'hover.mp3'
  },
  {
    id: 40,
    name: 'one',
    url: Constants.LETTER_IMAGE_URL_SEVEN + '40' + '.png',
    audioUrl: Constants.LETTER_IMAGE_URL_SEVEN + 'audio/' + 'hover.mp3'
  },
  {
    id: 53,
    name: 'one',
    url: Constants.LETTER_IMAGE_URL_SEVEN + '53' + '.png',
    audioUrl: Constants.LETTER_IMAGE_URL_SEVEN + 'audio/' + 'hover.mp3'
  },
  {
    id: 52,
    name: 'one',
    url: Constants.LETTER_IMAGE_URL_SEVEN + '52' + '.png',
    audioUrl: Constants.LETTER_IMAGE_URL_SEVEN + 'audio/' + 'hover.mp3'
  },
  {
    id: 51,
    name: 'one',
    url: Constants.LETTER_IMAGE_URL_SEVEN + '51' + '.png',
    audioUrl: Constants.LETTER_IMAGE_URL_SEVEN + 'audio/' + 'hover.mp3'
  },
  {
    id: 50,
    name: 'one',
    url: Constants.LETTER_IMAGE_URL_SEVEN + '50' + '.png',
    audioUrl: Constants.LETTER_IMAGE_URL_SEVEN + 'audio/' + 'hover.mp3'
  },
  {
    id: 49,
    name: 'one',
    url: Constants.LETTER_IMAGE_URL_SEVEN + '49' + '.png',
    audioUrl: Constants.LETTER_IMAGE_URL_SEVEN + 'audio/' + 'hover.mp3'
  },
  {
    id: 48,
    name: 'one',
    url: Constants.LETTER_IMAGE_URL_SEVEN + '48' + '.png',
    audioUrl: Constants.LETTER_IMAGE_URL_SEVEN + 'audio/' + 'hover.mp3'
  },
  {
    id: 47,
    name: 'one',
    url: Constants.LETTER_IMAGE_URL_SEVEN + '47' + '.png',
    audioUrl: Constants.LETTER_IMAGE_URL_SEVEN + 'audio/' + 'hover.mp3'
  },
];
