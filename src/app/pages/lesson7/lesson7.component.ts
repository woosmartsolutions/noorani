import {AfterViewInit, Component, ElementRef, HostListener, OnInit, ViewEncapsulation} from '@angular/core';
import {LessonSevenSevenLetterData, LessonSevenSixLetterData} from './lesson-seven';
import * as $ from 'jquery';
import {DataService} from 'src/app/services/data.service';

interface ImageIds {
  id: number;
}

@Component({
  selector: 'app-lesson7',
  templateUrl: './lesson7.component.html',
  styleUrls: ['./lesson7.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class Lesson7Component implements OnInit, AfterViewInit {
  sixLetterItems = LessonSevenSixLetterData;
  sevenLetterItems = LessonSevenSevenLetterData;
  isImageScaled = false;
  imageIds: ImageIds[] = [];
  isRangeClicked = false;
  isMediaStopped = false;
  oldValue = 0;
  newValue = 0;
  isPlaying = '';
  repeatCount = '';
  tArray = [];

  constructor(private ref: ElementRef, private dataService: DataService) {
  }

  ngOnInit() {
    this.setImageIds();

    this.dataService.getData().subscribe(res => {
      this.onRangeClick(res);
    });

    this.dataService.getOnStopStatus().subscribe(res => {
      this.stopMedia(res);
    });
  }

  ngAfterViewInit(): void {
    this.isPlaying = localStorage.getItem('isPlaying');
    this.repeatCount = localStorage.getItem('repeatCount');

    console.log(this.isPlaying, this.repeatCount);
  }

  setImageIds() {
    for (let i = 1; i <= 50; i++) {
      this.imageIds.push({id: i});
    }
  }

  onImageLetterClick(id?, counter?) {
    console.log(counter);
    let tId = 0;
    const imgItem = $('.letter-element');
    if (this.isRangeClicked) {
      $('#' + id).addClass('image-selected');
      tId = counter;
      if (this.newValue === 0) {
        this.newValue = tId;
      } else {
        this.oldValue = tId;
      }

      this.tArray = [];
      for (let i = this.newValue; i <= this.oldValue; i++) {
        this.tArray.push({
          id: this.sixLetterItems[i].id,
          name: this.sixLetterItems[i].name,
          url: this.sixLetterItems[i].url,
          audioUrl: this.sixLetterItems[i].audioUrl,
        });
      }
      // console.log('new Value: ' + this.newValue + '\nOld Value: ' + this.oldValue);
      console.log(this.tArray);
      setTimeout(() => {
        this.startPlaying();
      }, 5000);
      // newVal = counter;
      // if (newVal >= oldVal || newVal <= oldVal) {
      //   newVal = counter;
      // } else {
      //   oldVal = counter;
      // }
      // if (newVal === counter) {
      //   console.log('Please select different letter');
      //
      // }
      //
      this.sixLetterItems.forEach((item) => {
        if (id === item.id) {
          const url = item.audioUrl;
          console.log(url);
          $(url).play();
        }
      });

    } else {
      $(imgItem).removeClass('image-letter-scaled');
      this.isImageScaled = !this.isImageScaled;
      let duration;
      this.sixLetterItems.forEach((item) => {
        if (id === item.id) {
          $('#' + id).addClass('image-letter-scaled');
          const url = document.getElementById('audio' + id) as HTMLAudioElement;
          duration = url.duration;
          setTimeout(() => {
            url.pause();
            url.play();
          }, duration);
        }
      });

      this.sevenLetterItems.forEach((item) => {
        if (id === item.id) {
          $('#' + id).addClass('image-letter-scaled');
        }
      });
    }
  }

  @HostListener('document:click')
  onDocumentClick() {
    if (!this.ref.nativeElement.contains(event.target)) {
      const imgItem = $('.letter-element');
      $(imgItem).removeClass('image-letter-scaled');
    }
  }

  onRangeClick(res) {
    this.isRangeClicked = res;
    if (res) {
      const imgItem = $('.letter-element');
      $(imgItem).removeClass('image-letter-scaled');


    } else {
      const imgItem = $('.letter-element');
      $(imgItem).removeClass('image-letter-scaled');
    }
  }

  private stopMedia(res) {
    if (res) {
      this.isMediaStopped = true;
    } else {
      this.isMediaStopped = false;
    }
  }

  private startPlaying() {
    let duration;
    let url;
    const timer = setInterval(() => {
      for (let i = 0; i < this.tArray.length; i++) {
        $('#' + this.tArray[i].id).addClass('image-letter-scaled');
        url = document.getElementById('audio' + this.tArray[i].id) as HTMLAudioElement;
        duration = url.duration;
        if (this.isMediaStopped) {
          $('#' + this.tArray[i].id).removeClass('image-letter-scaled');
          url.pause();
        } else {
          url.play();
        }
        if (i === this.tArray.length) {
          clearInterval(timer);
        }
      }
    }, duration);
  }
}
