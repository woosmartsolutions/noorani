import {Routes} from '@angular/router';
import {Lesson1Component} from '../pages/lesson1/lesson1.component';
import {Lesson7Component} from '../pages/lesson7/lesson7.component';
import {SplashComponent} from '../splash/splash.component';
import {Lesson0Component} from '../pages/lesson0/lesson0.component';


export const LessonsRoutes: Routes = [
  {
    path: '', component: Lesson0Component
  },
  {
    path: 'lesson-one', component: Lesson1Component
  },
  {
    path: 'lesson-two', component: Lesson1Component
  },
  {
    path: 'lesson-three', component: Lesson1Component
  },
  {
    path: 'lesson-four', component: Lesson1Component
  },
  {
    path: 'lesson-five', component: Lesson1Component
  },
  {
    path: 'lesson-six', component: Lesson1Component
  },
  {
    path: 'lesson-seven', component: Lesson7Component
  },
  {
    path: 'lesson-eight', component: Lesson1Component
  },
  {
    path: 'lesson-nine', component: Lesson1Component
  },
  {
    path: 'lesson-ten', component: Lesson1Component
  },
  {
    path: 'lesson-eleven', component: Lesson1Component
  },
  {
    path: 'lesson-twelve', component: Lesson1Component
  },
  {
    path: 'lesson-thirteen', component: Lesson1Component
  },
  {
    path: 'lesson-fourteen', component: Lesson1Component
  },
  {
    path: 'lesson-fifteen', component: Lesson1Component
  },
  {
    path: 'lesson-sixteen', component: Lesson1Component
  },
  {
    path: 'lesson-seventeen', component: Lesson1Component
  },
];
