import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {Lesson1Component} from '../pages/lesson1/lesson1.component';
import {RouterModule} from '@angular/router';
import {LessonsRoutes} from './lesson-routing.module';
import {Lesson7Component} from '../pages/lesson7/lesson7.component';
import {LessonsDialogComponent} from '../dialog/lessons-dialog/lessons-dialog.component';
import {Lesson0Component} from '../pages/lesson0/lesson0.component';

@NgModule({
  declarations: [
    Lesson0Component,
    Lesson1Component,
    Lesson7Component,
    LessonsDialogComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(LessonsRoutes),
  ],
  entryComponents: [
    LessonsDialogComponent
  ]
})
export class LessonsModule {
}
