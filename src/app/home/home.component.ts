import { Component, HostListener, OnInit, Input } from '@angular/core';
import * as AOS from 'aos';
import { MatDialog, MatDialogConfig, MatDialogRef, MatSidenav } from '@angular/material';
import { LessonsDialogComponent } from '../dialog/lessons-dialog/lessons-dialog.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  mobileView = false;
  private lessonDialogRef: MatDialogRef<LessonsDialogComponent>;


  @Input() rangeSelected;

  constructor(private dialog: MatDialog) {
  }

  ngOnInit() {
    AOS.init();
    this.toggleMobileView();

    if (this.rangeSelected) {
      console.log(this.rangeSelected);
    }
  }

  @HostListener('window:resize', ['$event'])
  toggleMobileView() {
    console.log(window.innerWidth);
    if (window.innerWidth < 480) {
      this.mobileView = true;
    } else {
      this.mobileView = false;
    }
  }

  toggleSideMenu(sidenav: MatSidenav) {
    if (sidenav.opened) {
      sidenav.close();
    } else {
      sidenav.open();
    }
  }

  toggleLessonMenu(lessons) {
    const dialogConfig: MatDialogConfig = new MatDialogConfig<LessonsDialogComponent>();

    this.lessonDialogRef = this.dialog.open(LessonsDialogComponent, dialogConfig);
    this.lessonDialogRef.afterClosed().subscribe((res) => {
      console.log(res);
    });
  }
}
